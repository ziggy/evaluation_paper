function Plot_symbolic_var_changes(sym_fun, q0, MV0, q_sym, sym_var_1, var_range_1, sym_var_2, var_range_2)
%PLOT_SYMBOLIC_VAR_CHANGES Show multiple plots of effect of changing
%variable to the scalar input function

try % try whether sym_fun can be called as matlabFunction
    x0 = sym_fun(q0)
catch %if not, make it a matlabFunction of q
    sym_fun = matlabFunction(sym_fun, 'Vars',{q_sym, MV})
    x0 = sym_fun(q0, MV0)
end

%begin figure
figure
hold on

color = winter(length(var_range_2))

% vary in both ranges, and find state q
for j = 1:length(var_range_2)
    for i = 1:length(var_range_1)
        q = q0;
        q(q_sym == sym_var_1) = var_range_1(i);
        try %ignore second variable if it is not given, but do throw error if first one does not work
            q(q_sym == sym_var_2) = var_range_2(j);
        catch
            disp('No valid second variable present')
        end
        
        % calculate function value from q
        x(i) = sym_fun(q, MV0);
    end
    % plot line along variable 1 for given variable 2
    line(j) = plot(var_range_1, x, 'Color', color(j,:), 'DisplayName', (string(sym_var_2) + " = " + string(var_range_2(j))));
end

% plot line for q0
for i = 1:length(var_range_1)
    q = q0;
    q(q_sym == sym_var_1) = var_range_1(i);

    % calculate function value from q
    x(i) = sym_fun(q, MV0);
end
% plot line along variable 1 for given variable 2
line_q0 = plot(var_range_1, x, 'r', 'DisplayName', (string(sym_var_2) + " = " + q0(q_sym == sym_var_2)) + " (q0)");


% plot initial value
plot(q0(q_sym == sym_var_1), sym_fun(q0, MV0), 'ro', 'DisplayName', "q0")
legend
xlabel(string(sym_var_1))
ylabel("function value")



