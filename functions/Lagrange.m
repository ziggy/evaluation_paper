function sol = Lagrange(sys, energy, q_sym, qd_sym, Q_sym)
%LAGRANGE apply Lagrange for parameters in sys structure
% required parameters for sys:
%   - Tq
%   - Td
%   - M
%   - Cq
%   - Cd
%   - Cdd
%   - Sq
%   - Sd
%   - Sdd
%   - F

disp('Lagrange: start energy derivatives')
toc
T = energy.T_sym;
V = energy.V_sym;

% calculate energy derivatives
dT_dqd = simplify(jacobian(T, qd_sym));
dT_dqdq = simplify(jacobian(dT_dqd, q_sym));
dT_dq = simplify(jacobian(T, q_sym));
dV_dq = simplify(jacobian(V, q_sym));

% generalized mass matrix and generalized force
M_generalized = sys.Tq' * sys.M * sys.Tq; 
Fi = dT_dq'-dV_dq'-dT_dqdq*qd_sym; 

% zero matrices
zeroskk = zeros(size(sys.Cq,1));
zeroshh = zeros(size(sys.Sq,1));
zeroshk = zeros(size(sys.Cq,1),size(sys.Sq,1));

% setup Lagrange system
M_system = [M_generalized, sys.Cq', sys.Sq'; sys.Cq, zeroskk, zeroshk; sys.Sq, zeroshk', zeroshh];
%M_system = simplify(M_system)

Q_system = [Fi+Q_sym; -sys.Cdd; -sys.Sdd];  % for time invariant constraints!
%Q_system = simplify(Q_system)

disp('Lagrange: start solving system')
toc
% solve Lagrange system
% output = (M_system\Q_system);
output = rref([M_system, Q_system])
output = output(:,end)

disp('Lagrange: system solved')
toc
% split solution
qdd = output(1:length(q_sym));
lambda = output(length(q_sym)+1:end);

% state derivative
Y = [q_sym; qd_sym];
Yd = [qd_sym; qdd];

% save solutions as functions
sol.output = matlabFunction(output,'Vars',{q_sym,qd_sym,Q_sym});
sol.qdd = matlabFunction(qdd,'Vars',{q_sym,qd_sym,Q_sym});
sol.lambda = matlabFunction(lambda,'Vars',{q_sym,qd_sym,Q_sym});
sol.Yd = matlabFunction(Yd,'Vars',{Y,Q_sym});
sol.M_system = M_system;
sol.Q_system = Q_system;

% save solutions as symbolic expressions
sol.output_sym = output
end

