function tilde = tilde(x)
% generate tilde matrix for vector x.
tilde = [0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
end

