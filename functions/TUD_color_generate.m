function color = TUD_color_generate()
%TUD_COLOR_GENERATE Generate rgb vectors for TU Delft housestyle colors
color.cyan = [0 166 214]'/255;
color.black = [0 0 0]'/255;
color.white = [255 255 255]'/255;

color.F = [0 102 162]'/255;
color.D = [0 163 144]'/255;
color.R = [130 215 198]'/255;
color.B = [241 190 62]'/255;
color.C = [195 49 47]'/255;
color.A = [235 114 70]'/255;
color.N = color.cyan;
end

