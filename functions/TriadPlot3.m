function TriadPlot3(pos,NrotX,L,varargin)
x = pos + NrotX * 1.1 * [L;0;0];
y = pos + NrotX * [0;L;0];
z = pos + NrotX * [0;0;L];

LinePlot3([pos,x]',varargin{:})
% LinePlot3([x]','.',varargin{:})
LinePlot3([pos,y]',varargin{:})
LinePlot3([pos,z]',varargin{:})
end

