function outputstruct = Structconv_fun2num(inputstruct, varargin)
%STRUCTCONV_FUN2NUM Convert structure filled with matlab equations to
%structure filled with numeric values
%   Detailed explanation goes here

fn = fieldnames(inputstruct);
for k=1:numel(fn)
    field = inputstruct.(char(fn(k)));
    if isnumeric(field)
        outputstruct.(char(fn(k))) = field;
    elseif isstruct(field)
        outputstruct.(char(fn(k))) = field;
    else
        equation = inputstruct.(char(fn(k)));
        input = varargin{:};
        outputstruct.(char(fn(k))) = equation(varargin{:});
    end
end
end

