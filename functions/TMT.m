function sol = TMT(sys, q_sym, qd_sym, Q_sym)
%TMT Apply TMT method with parameters in sys structure
% required parameters for sys:
%   - Tq
%   - Td
%   - M
%   - Cq
%   - Cd
%   - Cdd
%   - Sq
%   - Sd
%   - Sdd
%   - F

% Convective accelerations
gk = jacobian(sys.Td,q_sym)*qd_sym;

% generalized mass matrix
M_generalized = sys.Tq' * sys.M * sys.Tq;

% zero matrices
zeroskk = zeros(size(sys.Cq,1));
zeroshh = zeros(size(sys.Sq,1));
zeroshk = zeros(size(sys.Cq,1),size(sys.Sq,1));

%% setup TMT system
disp('TMT: start generating rhs and lhs')
toc
M_system = [M_generalized, sys.Cq', sys.Sq'; sys.Cq, zeroskk, zeroshk; sys.Sq, zeroshk', zeroshh];
M_system = simplify(M_system)
Q_system = [Q_sym + sys.Tq'*(sys.F - sys.M*gk - sys.rotconv); -sys.Cdd; -sys.Sdd];
Q_system = simplify(Q_system)

disp('TMT: start solving system')
toc
% solve TMT system
output = (M_system\Q_system);

disp('TMT: system solved')
toc
% split solution
qdd = output(1:length(q_sym));
lambda = output(length(q_sym)+1:end);

% state derivative
Y = [q_sym; qd_sym];
Yd = [qd_sym; qdd];

% save solutions as functions
sol.output = matlabFunction(output,'Vars',{q_sym,qd_sym,Q_sym});
sol.qdd = matlabFunction(qdd,'Vars',{q_sym,qd_sym,Q_sym});
sol.lambda = matlabFunction(lambda,'Vars',{q_sym,qd_sym,Q_sym});
sol.Yd = matlabFunction(Yd,'Vars',{Y,Q_sym});
sol.M_system_sym = M_system;
sol.Q_system_sym = Q_system;

% save solutions as symbolic expressions
sol.output_sym = output;
end

