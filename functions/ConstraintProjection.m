function [q_n1, qd_n1] = ConstraintProjection(var, sys, q0, qd0, MV0)
iterat = 0;
tol = 1e-12;
q_n1 = q0;
qd_n1 = qd0;
maxiterat = 1000;

if ~isempty(sys.C)
    %% Positions projection
    Cq = sys.Cq_func(q0, MV0);
    C = sys.C_func(q0, MV0);

    % Loop until accuracy is achieved
    while max(max(abs(C))) > tol && iterat < maxiterat
        Dq_n1 = -Cq'*((Cq*Cq')^(-1))*C;
        q_n1 = q_n1 + Dq_n1;

        Y = [q_n1;qd_n1];
        Cq = sys.Cq_func(q_n1, MV0);
        C = sys.C_func(q_n1, MV0);
        iterat = iterat + 1;
    end

    %%  Velocity projection
    % update constraints for found state
    if ~isempty(sys.S)
        Cq = sys.Cq_func(q_n1, MV0);
        C = sys.C_func(q_n1, MV0);

        S = sys.S_func(q_n1, qd_n1, MV0);
        Sq = sys.Sq_func(q_n1, qd_n1, MV0);

        % time derivative of holonomic constriants
        Cd = Cq*qd_n1;

        % determine new derivative of q
        err = [Cd; S];
        Jkj = [Cq; Sq];

        Delta = pinv(Jkj)*(-err);
        qd_n1 = qd_n1 + Delta;
    else
        disp('ConstraintProjection: S is empty, so velocity is not projected')
    end
else
    disp('ConstraintProjection: C is empty, so position and velocity are not projected')
end

%% final state
Y = [q_n1;qd_n1];

end

