function outputstruct = Structconv_sym2fun(inputstruct, varargin)
%STRUCTCONV_SYM2FUN Convert structure filled with symbolic expressions to
%structure filled with matlab equations
%   Detailed explanation goes here

% https://nl.mathworks.com/matlabcentral/answers/341454-how-to-loop-over-the-structure-fields-and-get-the-type-of-data
fn = fieldnames(inputstruct);
for k=1:numel(fn)
    field = inputstruct.(char(fn(k)));
    
    if isnumeric(field)
        outputstruct.(char(fn(k))) = field;
    elseif isstruct(field)
        outputstruct.(char(fn(k))) = field;
    else
        try
            outputstruct.(char(fn(k))) = matlabFunction(inputstruct.(char(fn(k))), 'Vars', varargin);
        catch
            disp(char(fn(k)) + " could not be converted to Matlab Funciton")
        end
    end
end
end

