function x = un_tilde(A)
% reshape tilde matrix into vector

if ~isnumeric(A) || all(diag(A) == zeros(3,1)) && A(2,3) == -A(3,2) && A(1,2) == -A(2,1) && A(1,3) == -A(3,1)
    x(1,1) = A(3,2); % store values of x if A is symbolic
    x(2,1) = A(1,3); % skew symmetry of symbolic matrix is not guaranteed!
    x(3,1) = A(2,1);    
end

