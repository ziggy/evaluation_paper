function [var] = variables(var, GC_chosen, castor_variable_set, GC_initial_condition_override, mutable_variable_names)
%% CastorModel_variables defines the var struct with the following constant parameters:
%     sigma
%     phi_x
%     phi_y
%     r_w
%     L_ex
%     L_ey
%     L_df 
% If the parameters are changed, the pos structure must be recalculated
% using CastorModel_derive_pos. Otherwise the model will still use the old
% parameters for the determination of the positions

%% Parameters for figures
var.Ltriad = 0.04; % length of lines used to plot triads

%% Store relevant GC variables. Initial values are set at end of code
% GC_names contains names of all potential GC, which can be cross
%  refererenced with the input of chosen GC
GC_names = ["N_xB" "N_yB" "N_zB" "N_xD" "N_yD" "N_zD" "zetax" "zetay" "zetaz" "delta" "gamm" "epsilon" "theta" "eta"];
GC_membership = ismember(GC_names, GC_chosen);

% GCpotential contains all potential GC
syms N_xB N_yB N_zB N_xD N_yD N_zD zetax zetay zetaz delta gamm epsilon theta eta real
GC_potential = [N_xB N_yB N_zB N_xD N_yD N_zD zetax zetay zetaz delta gamm epsilon theta eta];

% GCd_potential contains derivatives of all potential GC
syms N_xBd N_yBd N_zBd N_xDd N_yDd N_zDd zetaxd zetayd zetazd deltad gammd epsilond thetad etad real
GCd_potential = [N_xBd N_yBd N_zBd N_xDd N_yDd N_zDd zetaxd zetayd zetazd deltad gammd epsilond thetad etad];

% GCpotential contains a sym for GC variables and a double for constants
syms QN_xB QN_yB QN_zB QN_xD QN_yD QN_zD Qzetax Qzetay Qzetaz Qdelta Qgamm Qepsilon Qtheta Qeta real
GCQ_potential = [QN_xB QN_yB QN_zB QN_xD QN_yD QN_zD Qzetax Qzetay Qzetaz Qdelta Qgamm Qepsilon Qtheta Qeta];

% Add time to support prescribed motion
syms t real positive
var.t = t;

var.g = 9.81;

%% Wheelchair variables
var.P_f = 0.33;

var.a_P = -0.1;
var.a_r1 = 0;
var.a_r2 = var.a_r1;
var.a_f1 = 0;
% var.a_G = var.P_f*var.a_f1; See bottom

var.b_G = 0;
var.b_r1 = 0.565/2;
var.b_r2 = -var.b_r1;
var.b_f1 = 0;
var.b_P = var.b_r1; %it seems possible to push the wheelchair while walking next to it by grabbing the bar above the rear wheel

var.r_r1 = 0.56/2; %https://www.firstinarchitecture.co.uk/metric-data-03-average-dimensions-of-wheelchair-user/
var.r_r2 = var.r_r1;
var.r_f1 = 0.190/2; %radius of front wheel

ignoreRollResist = 0;
switch ignoreRollResist
    case 0
        var.f_r_fi = 0.025; %coefficient of rolling resistance front wheels, based on measured castor wheel in the first experiment
        var.f_r_ri = 0.005; %coefficient of rolling resistance rear wheels
    case 1
        var.f_r_fi = 0; %coefficient of rolling resistance front wheels
        var.f_r_ri = 0; %coefficient of rolling resistance rear wheels
end

vertical_load = 1;



%% Default GC values
% initialize all GC as 0 by default. Variable sets can override this
N_xD = 0;
N_yD = 0;
N_zD = 0;
N_xB = 0;
N_yB = 0;
N_zB = -var.r_r1;
N_zB = 0;
zetax = deg2rad(0);
zetay = deg2rad(0);
zetaz = deg2rad(0);
delta = deg2rad(0);
gamm = deg2rad(0);
epsilon = deg2rad(0);
theta = deg2rad(0);
eta = deg2rad(0);

N_xDd = 0;
N_yDd = 0;
N_zDd = 0;
N_xBd = 0;
N_yBd = 0;
N_zBd = 0;
zetaxd = deg2rad(0);
zetayd = deg2rad(0);
zetazd = deg2rad(0);
deltad = deg2rad(0);
gammd = deg2rad(0);
epsilond = deg2rad(0);
thetad = deg2rad(0);
etad = deg2rad(0);

%% Default MV values
var.r_turn = 1000;
var.v_wc = 0.8; % forward velocity of the wheelchair
var.M_z_wheelchair = 0; %moment applied in Z direction on wheelchair center. 
% var.F_G = 1000; %total force caused by weigth of wheelchair and occupant

%names of mv variables:
var.mutable_variable_names = mutable_variable_names;

%% Define castor variables for each case
switch castor_variable_set
    case 'sym'
        syms delta phi_x phi_y r_w L_ex L_ey L_df sigma real
    case 'rand'
        sigma = deg2rad(-40+rand()*80);
        phi_x = deg2rad(-30+rand()*60);
        phi_y = deg2rad(-30+rand()*60);
        r_w = 5/100+rand()*10/100;
        L_ex = rand()*20/100;
        L_ey = rand()*5/100;
        L_df = rand()*20/100;
    case 12 % SET 1: based on measurements of wheelchair in the lab
        sigma = deg2rad(0);
        phi_x = deg2rad(0);
        phi_y = deg2rad(0);
        r_w = 0.190/2;
        L_ex = 0.05;
        L_ey = 0.00;
        L_df = 0.175;

    case 13 % SET 2: based on measurements of wheelchair in the lab with nonzero cant
        sigma = deg2rad(0);
        phi_x = 0.1;
        phi_y = deg2rad(0);
        r_w = 0.190/2;
        L_ex = 0.05;
        L_ey = 0.00;
        L_df = 0.175;
end

phi_z = 0;

%% NEW GC method
switch GC_initial_condition_override
    case 1 % test overriding inital conditions
        N_xD = 0;
        N_xDd = 0;
        delta = deg2rad(0);
        N_zD = 0;
end
        


%% Store generalized coordinates in var
var.N_xD = N_xD;
var.N_yD = N_yD;
var.N_zD = N_zD;
var.N_xB = N_xB;
var.N_yB = N_yB;
var.N_zB = N_zB;
var.delta = delta;
var.theta = theta;
var.zetax = zetax;
var.zetay = zetay;
var.zetaz = zetaz;
var.gamm = gamm;
var.epsilon = epsilon;
var.eta = eta;

var.N_xDd = N_xDd;
var.N_yDd = N_yDd;
var.N_zDd = N_zDd;
var.N_xBd = N_xBd;
var.N_yBd = N_yBd;
var.N_zBd = N_zBd;
var.deltad = deltad;
var.thetad = thetad;
var.zetaxd = zetaxd;
var.zetayd = zetayd;
var.zetazd = zetazd;
var.gammd = gammd;
var.epsilond = epsilond;
var.etad = etad;

%% General variables (same for all cases)
var.g = 9.81; %gravitational constant

% List points that are required for dynamic calculations (will be evaluated
% for velocity and energy)
% Mandatory points:
%   - "C"     Contact point, required for contact constraint
% Optional points:
%   - "A"; "A2"; "D"; "F"; "CoMF"; "W"
var.point_name = ["B","D","F","A","W","C","G"];

% List triads that are required for dynamic calculations (will be evaluated
% for velocity and energy)
% Optional points:
%   - "B"; "C"; "F"; "W"
var.triad_name = ["B","W","F"];

%% Masses
var.mF = 0; %mass of castor fork
var.mW = 0; %mass of castor wheel
var.mD = 0; %mass of structure between castor fork and force sensor
var.mB = 0; 
var.mG = 1000/9.81; %mass of wheelchair and occupant

% mass matrices
var.MF = var.mF * eye(3);
var.MW = var.mW * eye(3);
var.MD = var.mD * eye(3);
var.MB = var.mB * eye(3);
var.MG = var.mG * eye(3);

%% center of mass (manual approximation!)
% castor fork
F_rCoMF_F_x = -0*L_ex/2; %position of CoM of fork wrt point F in x direction
F_rCoMF_F_y = -0*L_ey/2;
F_rCoMF_F_z = -0*L_df/3;
var.F_rCoMF_F = [F_rCoMF_F_x; F_rCoMF_F_y; F_rCoMF_F_z];

% wheel
% in point A, known directly

% vehicle connection
% in point D, known directly

% wheelchair
% in point B, known directly

%% Rotational inertia matrices
% castor fork
F_IF_xx = 0;
F_IF_yy = 0;
F_IF_zz = 0;
var.F_IF = diag([F_IF_xx, F_IF_yy, F_IF_zz]);

% castor wheel, approximate as disk
W_IW_xx = 1/2 * var.mW * r_w;
W_IW_yy = var.mW * r_w;
W_IW_zz = W_IW_xx;
var.W_IW = diag([W_IW_xx, W_IW_yy, W_IW_zz]);

% vehicle connection
B_ID_xx = 0;
B_ID_yy = 0;
B_ID_zz = 0;
var.B_ID = diag([B_ID_xx, B_ID_yy, B_ID_zz]);

% wheelchair 
B_IB_xx = 0;
B_IB_yy = 0;
B_IB_zz = 0;
var.B_IB = diag([B_IB_xx, B_IB_yy, B_IB_zz]);

%% Applied external forces
%gravity
N_FD_g = [0; 0; var.mD*var.g];
N_FA_g = [0; 0; var.mW*var.g]; % apply force of wheel at point A
N_FF_g = [0; 0; var.mF*var.g];
N_FB_g = [0; 0; var.mB*var.g];

%totals
var.N_FD = N_FD_g;
var.N_FA = N_FA_g;
var.N_FF = N_FF_g;
var.N_FB = N_FB_g;

%% Applied external torques
var.N_TD = [0;0;0];
var.N_TF = [0;0;0];

%% Store variables in var structure
var.sigma = sigma;
var.phi_x = phi_x;
var.phi_y = phi_y;
var.phi_z = phi_z;
var.r_w = r_w;
var.L_ex = L_ex;
var.L_ey = L_ey;
var.L_df = L_df;
var.vertical_load = vertical_load;

%% Save GC
% initialize empty vectors
q_sym = [];
qd_sym = [];
q0 = [];
qd0 = [];
Q_sym = [];
Q0 = [];

% store symbolic and initial values of GC in membership
for i = 1:length(GC_potential)
    if GC_membership(i) == 1
        q_sym = [q_sym; GC_potential(i)];
        qd_sym = [qd_sym; GCd_potential(i)];
        
        q0 = [q0; var.(strcat(GC_names(i)))];
        qd0 = [qd0; var.(strcat(GC_names(i),'d'))];
        
        Q_sym = [Q_sym; GCQ_potential(i)];
        Q0 = [Q0; 0];
        
        var.(strcat(GC_names(i))) = GC_potential(i);
        var.(strcat(GC_names(i),'d')) = GCd_potential(i);
    end
end

% store GC dependent data in var
var.q_sym = q_sym;
var.qd_sym = qd_sym;

var.q0 = q0;
var.qd0 = qd0;

var.Q_sym = Q_sym;
var.Q0 = zeros(size(Q_sym));

%% Replace mutable variables in var structure and store the original values in MV0
MV0 = [];
for i = 1:length(mutable_variable_names)
    MV0(i,:) = var.(string(mutable_variable_names(i)));
    var.(string(mutable_variable_names(i))) = sym(mutable_variable_names(i), 'real');
    MV(i,:) = var.(string(mutable_variable_names(i)));
end
var.MV = MV; %store MV in var as well, this makes it easier for some functions to access it
var.MV0 = MV0;

%% Variables that depend explicitly on MV must be placed here
% var.vertical_load = var.mG * var.P_f * var.g; %vertical load on castor wheel in Newton, can be used as replacement for weight without inertial effects.
var.a_G = var.P_f*var.a_f1;
var.M_t = var.vertical_load * var.f_r_fi * var.r_w;








