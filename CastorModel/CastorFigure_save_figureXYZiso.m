function CastorFigure_save_figureXYZiso(fignum, name, dimension)
%CASTORFIGURE_SAVE_FIGUREXYZISO Summary of this function goes here
%   Detailed explanation goes here

% make directory if it does not exist yet
name_split = split(name, '/');
if length(name_split) > 1
    path = "";
    for i = 1:(length(name_split)-1)
        path = path + name_split(i) + "/";
    end
    mkdir(path)
end

if exist(fignum, "var")
    figure(fignum)
end
saveas(gca, name + ".fig")

if dimension == 3
    % save figures
    saveas(gca, name + "_isometric.pdf")
%     saveas(gca, name + "_isometric.svg")
    view(0,0)
    saveas(gca, name + "_XZ.pdf")
%     saveas(gca, name + "_XZ.svg")
    view(0,90)
    saveas(gca, name + "_XY.pdf")
%     saveas(gca, name + "_XY.svg")
    view(-90,0)
    saveas(gca, name + "_YZ.pdf")
%     saveas(gca, name + "_YZ.svg")
    
elseif dimension == 2
    saveas(gca, name + ".pdf")
    saveas(gca, name + ".png")
    % https://stackoverflow.com/questions/5150802/how-to-save-a-plot-into-a-pdf-file-without-a-large-margin-around/6069881
%     set(gcf, 'PaperPosition', [0 0 5 4.5]); %Position plot at left hand corner with width 5 and height 5.
%     set(gcf, 'PaperSize', [5 4.5]); %Set the paper to have width 5 and height 5.
    fig = gcf;
    fig.PaperPositionMode = 'auto';
    fig_pos = fig.PaperPosition;
    fig.PaperSize = [fig_pos(3) fig_pos(4)]
    saveas(gcf, name, 'pdf') %Save figure
%     saveas(gca, name + ".svg")
    
else
    disp('save_figure: figure was only saved as .fig file')
end

end

