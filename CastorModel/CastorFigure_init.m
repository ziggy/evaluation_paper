function fig_castor = CastorFigure_init(fignum)
% Initialize a coordinate system with z pointing downward.
% The origin is plotted at 0,0,0
% close(figure(fignum))
fig_castor = figure(fignum);
plot3(0,0,0,'.k');
xlabel('x / m');
ylabel('y / m');
zlabel('z / m');
axis equal;
fig_castor.CurrentAxes.ZDir = 'Reverse';
fig_castor.CurrentAxes.YDir = 'Reverse';

hold on;

end

