function [kin] = derive_kin(var)
%% derive_kin calculates the kinematics (position and rotation) of the castor wheel.
% This function was split in pos and rot in previous versions of the code.

addpath('../Functions')

%% Rotations dependent on var or q directly
% vehicle connection wrt inertial
kin.BrotN = rotx(var.zetax)' * roty(var.zetay)' * rotz(var.zetaz)';
kin.NrotB = kin.BrotN';

% phi between swivel axis and vehicle connection
kin.DrotB = rotx(var.phi_x)' * roty(var.phi_y)' * rotz(var.phi_z)';
kin.BrotD = kin.DrotB';
kin.NrotD = kin.NrotB * kin.BrotD;

% swivel angle
kin.RrotD = rotz(var.delta)';
kin.DrotR = kin.RrotD';

% sigma in castor fork
kin.FrotR = rotx(var.sigma)';
kin.RrotF = kin.FrotR';
kin.BrotF = simplify(kin.BrotD * kin.DrotR * kin.RrotF);
kin.NrotF = kin.NrotB * kin.BrotF;
kin.FrotN = kin.NrotF';

% Triad fixed to wheel
kin.WrotF = roty(var.theta)';
kin.FrotW = kin.WrotF';
kin.NrotW = kin.NrotF * kin.FrotW;

%% Calculate orientation of contact triad C
% Find important unit vectors
B_n = [0;0;-1];
B_s = kin.BrotF * [0;1;0];

% Project wheel spin axis s on ground plane
B_s_proj_ground = B_s - (dot(B_s,B_n) / norm(B_n,2)) * B_n;

% Normalize length of projected s, which is equal to the lateral wheel contact vector
B_t = simplify(B_s_proj_ground / norm(B_s_proj_ground));
B_l = simplify(cross(B_n,B_t));

kin.CrotB = simplify([B_l'; B_t'; -B_n']);
kin.BrotC = kin.CrotB';

kin.CrotF = simplify(kin.CrotB * kin.BrotF);
kin.FrotC = kin.CrotF';

kin.CrotN = simplify(kin.CrotB * kin.BrotN); 
kin.NrotC = kin.CrotN';

%% Calculate the prescribed angles
% Assume that the angle epsilon and gamma are small. This assumption cannot
% be made for eta.
% kin.epsilon = dot(B_l, kin.BrotF * [0;1;0]) - pi/2;
F_l = kin.FrotC * [1;0;0];
kin.epsilon = atan2(F_l(3), F_l(1));

% kin.gamma = -dot(B_s, B_n);
kin.gamma = -atan2(B_s'*B_n, B_s'*B_t)

N_l = kin.BrotN' * B_l;
kin.eta = atan2(N_l(2),N_l(1));

%% Castor model positions derivation
% Derives the symbolic functions for the locations of the points on the
% castor wheel.
% The functions are stored in the structure pos in the N triad.



%% Find vectors in C triad to determine r
% triad fixed vectors
N_n = [0;0;-1]; % equal to C_n
F_s = [0;1;0]; % equal to F_f2
F_n = kin.NrotF' * N_n;

% ground plane in C triad
C_l = [1;0;0];
% F_l = rot.FrotC * C_l
F_l = roty(-var.epsilon)' * [1;0;0]; %
F_l = simplify(cross(-F_s, F_n) / norm(cross(-F_s, F_n)));
F_t = simplify(cross(F_l, F_n));

% vector r connecting A and C
F_l_cross_s = cross(F_l,F_s);
F_r_hat = F_l_cross_s; % / norm(F_l_cross_s); No norm required, l and s are always orthogonal
F_r = F_r_hat * var.r_w;

% transform to B triad
kin.B_s = kin.BrotF * F_s;
kin.B_t = kin.BrotF * F_t;
kin.B_l = kin.BrotF * F_l;

% transform to N triad
N_r = simplify(kin.NrotF * F_r);
N_r_hat = kin.NrotF * F_r_hat;
N_l = kin.NrotF * F_l;
N_t = kin.NrotF * F_t;
N_s = kin.NrotF * F_s;

% Save calculations
%Ftriad
kin.F_t = simplify(F_t);
kin.F_r = simplify(F_r);
kin.F_l = simplify(F_l);
kin.F_n = simplify(F_n);
kin.F_s = F_s;

%Ntriad
kin.t = simplify(N_t);
kin.l = N_l;
kin.s = N_s;
kin.r = N_r;
kin.r_hat = N_r_hat;


%% Positions in N triad
% Origin of vehicle
N_rB = [var.N_xB; var.N_yB; var.N_zB];

% Center of mass
N_rG = [var.N_xB; var.N_yB; 0] + kin.NrotB * [var.a_G; 0; var.r_r1];

% Vehicle connection point D
N_rD = [var.N_xB; var.N_yB; 0] + kin.NrotB * [var.a_f1; var.b_f1; var.N_zD];

% Castor fork point F, A, and A2
N_rD_F = kin.NrotD * [0;0;-var.L_df];
N_rF = N_rD - N_rD_F;

N_rA2_F = kin.NrotF * [-var.L_ex;0;0];
N_rA2 = N_rF + N_rA2_F;

N_rA_A2 = kin.NrotF * [0; var.L_ey; 0];
N_rA = N_rA2 + N_rA_A2;

% Castor fork center of mass
N_rCoMF = kin.NrotF*var.F_rCoMF_F + N_rF;

% Contact point C
N_rA_C = -N_r;
N_rC = N_rA - N_rA_C;

%% Point C with respect to D in D triad
% Castor fork point F, A, and A2
D_rF_D = [0;0;var.L_df];

D_rA2_F = kin.DrotR * kin.RrotF * [-var.L_ex;0;0];
D_rA2_D = simplify(D_rF_D + D_rA2_F);

D_rA_A2 = kin.DrotR * kin.RrotF * [0; var.L_ey; 0];
D_rA_D = simplify(D_rA2_D + D_rA_A2);

% Contact point C
D_rC_A = kin.DrotR * kin.RrotF * F_r;
kin.D_rC_D = simplify(D_rA_D + D_rC_A);
kin.D_rA_D = simplify(D_rA_D);

%% Geometric points
% Geometric constrained contact point R (equal to C for correct gamma)
% [gamma,epsilon] = CastorModel_derive_gamma_epsilon(rot.NrotF)
% var.epsilon = epsilon
F_rR_A = roty(var.epsilon)' * [0;0;var.r_w];
N_rR_A = kin.NrotF * F_rR_A;
N_rR = N_rA + N_rR_A;

% Point T which is always fixed to the wheel and at the position described
% by theta
F_rT_A = roty(var.theta)' * [0;0;var.r_w];
N_rT_A = kin.NrotF * F_rT_A;
N_rT = N_rA + N_rT_A;

%% Determine h
disp('derive_pos: calulate and simplify h')
toc

% Determine the height of the contact point from the ground plane
% N_rC = simplify(N_rC);
N_rC_B = (N_rC - N_rD);

h = dot(N_rC_B,[0;0;1]);

%% finished calculations
disp('derive_pos: finished calculations, start storing matlabFunctions in pos structure')
toc

%% Save important vectors and values to structure
kin.h = h;

%% Save variables used to generate position functions
kin.var = var;

%% Save positions to pos structure
kin.B = N_rB;
kin.D = N_rD;
kin.F = N_rF;
kin.CoMF = N_rCoMF;
kin.A2 = N_rA2;
kin.A = N_rA;
kin.C = N_rC;
% pos.C_rC = rot.CrotN * N_rC;
kin.R = N_rR;
kin.F_R_A = F_rR_A;
kin.T = N_rT;
kin.G = N_rG;

% Assume that wheel is rotationally symmetric and therefore center of wheel
% coincides with A
kin.W = N_rA;

%% End of program
disp('derive_kin: finished')
toc
end




