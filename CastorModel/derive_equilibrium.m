function method = derive_equilibrium(var, kin)
%METHOD derive equilibrium: Derive symbolic approximation of moment on
%castor swivel angle.
%   Detailed explanation goes here

% introduce name for unknown lateral force. Vertical load is known from
% var structure
syms F_t F_n F_l epsilon real
F_l_num = -var.f_r_fi * var.vertical_load;

% Assumptions:
% - Castor fork and wheel have no weight, so vertical load is equal to normal force on wheel
% - Sigma and phi_x (cant) are small
% - Castor moves in steady state (delta constant)
% - Wheel never slips, even if lateral force is larger than friction coefficient times vertical load

% perform small angle approximaiton for lateral force on wheel
saa = @(sym, angle) simplify(subs(subs(sym, sin(angle), angle), cos(angle), 1)); %Small Angle Approximation

%% Calculate moment around D and use it to find lateral force on castor
C_F_C = [F_l; F_t; -var.vertical_load]; %force on point C in C triad
B_F_C = simplify(kin.BrotC * C_F_C); 
D_F_C = simplify(kin.DrotB * B_F_C);

D_rC_D = kin.D_rC_D; %position of C wrt D in D triad

% Moment around point D in D triad. NOTE: F_t in kinematics is vector t in triad F, not the lateral force
D_M_D = cross(D_rC_D, D_F_C) + kin.DrotR * kin.RrotF * kin.F_t * var.M_t; 

eq1 = D_M_D(3) == 0; %swivel axis cannot generate moment around axis of movement
C_F_C_y = simplify(solve(eq1, F_t)); %solve eq1 for lateral force

method.C_F_C_y = subs(C_F_C_y, F_l, F_l_num);
method.C_F_C = [F_l_num; method.C_F_C_y; -var.vertical_load];
method.F_M_D = - cross(kin.FrotR * kin.RrotD * kin.D_rC_D, kin.FrotC * method.C_F_C) - kin.F_t * var.M_t;

%% Cosine approximation
method.C_F_C_y_cosine = var.vertical_load * var.phi_x * cos(var.delta);

%% Simple calculation assuming that t == s - s(3)*n in the D triad such that more elements align with the coordinate system
% Note that this solution is NOT used in the main file; it is provided as
% an alternative small angle approximated equation that could be utilized.
D_n = kin.NrotD' * [0;0;-1];
D_s = kin.RrotD' * kin.FrotR' * kin.F_s;
D_t = D_s - D_n * dot(D_s, D_n);
% D_t = D_t / norm(D_t) % normalize, can be ignored for simpler solution

D_l = cross(D_n, D_t);
D_r = cross(D_l, D_s);

% D_rA_D = subs(pos_sym.A - pos_sym.D, [var.zetax; var.zetay; var.zetaz], [0;0;0]) <- MISTAKE!
% D_rC_D = B_rA_D + B_r*var.r_w
D_rA_D = kin.D_rA_D;
D_rC_D = D_rA_D + D_r*var.r_w;
D_rC_D = D_rA_D + kin.DrotR * kin.RrotF * roty(-epsilon)*[0;0;1]*var.r_w;

D_F_C = D_t * F_t + D_n*F_n;

% calculate moment around point D
switch 1
    case 1 %exact
        D_M_D = cross(D_rC_D, D_F_C) + var.M_t * D_t;
    case 2 %subs parts of rC_D, much easier to read
        syms a b c real
        D_rC_D_sym = [a;b;c];
        D_M_D = cross(D_rC_D_sym, D_F_C) + var.M_t * D_t;
end

D_d3 = [0;0;1];
D_M_D_z = dot(D_M_D, D_d3);
D_M_D_z = subs(D_M_D_z, var.phi_z, 0);
D_M_D_z = saa(D_M_D_z, var.phi_x);
D_M_D_z = saa(D_M_D_z, var.phi_y);
D_M_D_z = saa(D_M_D_z, var.sigma);
D_M_D_z = saa(D_M_D_z, epsilon);
D_M_D_z = simplify(D_M_D_z);
F_t = solve(D_M_D_z == 0, F_t);
F_t = simplify(F_t);

D_f3 = kin.DrotR * kin.RrotF * [0;0;1];
epsilon_calc = dot(D_l, D_f3) - pi/2;
epsilon_calc = saa(epsilon_calc, var.phi_x);
epsilon_calc = saa(epsilon_calc, var.phi_y);
epsilon_calc = saa(epsilon_calc, var.sigma);
% epsilon_calc = 0

method.C_F_C_y_small_angle_approx = subs(F_t, [epsilon; F_n], [-epsilon_calc; var.vertical_load]);

end

