%% main
% main runs the derivation of equations of motions for the
% castor wheel model based on the inputs given at the start of the code.
%   Date last edit: 2024-06-23
%   Author: Matto Leeuwis

%% Initialize
% point path to general functions
addpath('../Functions')
saa = @(sym, angle) simplify(subs(subs(sym, sin(angle), angle), cos(angle), 1)); %Small Angle Approximation
tic

%% Control center
% define generalized coordinates, choose from following set:
%   "N_xD"      x coordinate of point D wrt O
%   "N_yD"      y coordinate of point D wrt O 
%   "N_zD"      z coordinate of point D wrt O
%   "zetax"     x rotation of vehicle
%   "zetay"     y rotation of vehicle 
%   "zetaz"     z rotation of vehicle
%   "delta"     swivel angle
%   "gamma"      caster angle of wheel
%   "epsilon"   pitch of castor fork
%   "theta"     rotation of wheel wrt castor fork
%   "eta"       heading angle
GC_chosen = ["delta", "N_xB", "N_yB", "N_zD", "theta", "zetaz"];

% choose what variables are used as function input rather than a constant
% numeric value. Making a variable mutable means that it can be changed
% after the system has been solved, or even (discontinuously) during 
% simulation if desired for some reason.
mutable_variable_names = ["r_w", "L_ex", "L_ey", "L_df", "sigma", "phi_x", "phi_z", "r_turn", "a_f1", "b_f1", "v_wc", "M_z_wheelchair", 'f_r_fi', 'f_r_ri', 'P_f', 'g', 'mG', 'vertical_load']';

% castor_variable_set: Selects a set of parameters for the castor wheel such as wheel radius and mass etc.
%   'rand':     Random parameters within somewhat reasonable bounds
%   'sym':      Symbolic, will break matlabFunction generation in other functions
%   12:         Used for MSc thesis
%   13:         Treadmill experiment castor wheel  
castor_variable_set = 13;

% GC_initial_condition_override: enforce these initial conditions when
% finding a set of valid initial conditions. Later in the code an iterative
% process is used to ensure that initial conditions are valid, the
% conditions selected with this method will be coerced to their set value.
%   0:          Zero for all initial conditions
%   1:          Experimental set, change at will
GC_initial_condition_override = 1;

% generate variables based on choices
var = struct();
var = variables(var, GC_chosen, castor_variable_set, GC_initial_condition_override, mutable_variable_names);

% Choose simulation settings
method = "TMT"; %Pick TMT or L for Lagrange
var.evaluate_result = 1; %choose whether any integration or figure code is used
var.animate_bool = 0; %animate plots or display final result as fast as possible
var.target_frame_time = 1/30; %target frame time for animations, equal to 1/framerate
var.export_figures_bool = 1; %export figures to files

%% load symbolic expression of state, split in q and qd
q_sym = var.q_sym;
qd_sym = var.qd_sym;
Q_sym = var.Q_sym;

Y = [q_sym; qd_sym];

% load MV in workspace
MV = var.MV;
MV0 = var.MV0;

%% Kinematics
kin = derive_kin(var);
kin_fun = Structconv_sym2fun(kin, q_sym, var.MV);

%% Find solution based on steady state assumption
methods_1 = derive_equilibrium(var, kin);
methods_1_fun = Structconv_sym2fun(methods_1, q_sym, var.MV)




%% Vary a number of variables over delta and plot as function of angle between l and b1
path = "../../Figures_Simplified";

figureLayoutMode = "tiled"
switch figureLayoutMode
    case "tiled"
        figure(Name='Tiled castor results',Color=[1,1,1],Units="centimeters",Position=[5, 5, 18.3, 13.0]);
        tiledlayout(1,2); ax1 = nexttile; hold on
    otherwise
        figure; hold on
end

values_phi_x = [-0.2, -0.1, 0, 0.1, 0.2];
values_delta = linspace(-pi+0.001,pi-0.001,75);

name = "Swivel angle $\delta$ (rad)";
name_iterated_variable = "$\varphi_x$";

% initialize figure
%         F_t |
%         --- |
%         F_n |
%             |____________
%                  delta
col = turbo(11);
col = flipud(col(1:10,:));

col = [
    flipud(winter(4));
    0, 0, 0;
    autumn(4)
];

col = [
    flipud(autumn(4));
    0, 0, 0;
    winter(4)
];


set(gca,'colororder',col)
set(gca,'TickLabelInterpreter','latex')
posfig=get(gca,'position');  % retrieve the current values
posfig(3)=0.95*posfig(3);    % reduce width
set(gca,'position',posfig);  % write the new values

% add lines for reference
xline(pi/2, '-', "Neg. rake", 'HandleVisibility','off', 'LabelVerticalAlignment','bottom', 'Interpreter','latex', Color=[1;1;1]*0.8, FontSize=8)
xline(-pi/2, '-', "Pos. rake", 'HandleVisibility','off', 'LabelVerticalAlignment','bottom', 'Interpreter','latex', Color=[1;1;1]*0.8, FontSize=8)
xline(0, '-', "Pos. cant", 'HandleVisibility','off', 'LabelVerticalAlignment','bottom', 'Interpreter','latex', Color=[1;1;1]*0.8, FontSize=8)
% xline(0,':','HandleVisibility','off')
% yline(0,':','HandleVisibility','off')

% perform calculations
F_t = [];
F_t_cosine = [];

for i = 1:length(values_phi_x)
    MV0 = sym(var.MV0);
    MV0(var.MV == var.phi_x) = values_phi_x(i);

    q0 = sym(zeros(size(var.q_sym)));
    q0(var.q_sym == var.delta) = var.delta;

    F_t_fun = methods_1_fun.C_F_C_y(q0, MV0);
    M_D_fun = methods_1_fun.F_M_D(q0, MV0);
    B_l_fun = kin_fun.B_l(q0, MV0);
    
    delta_list = []; %list to store computed angle between vector l and x. By approximation this is equal to delta.
    
    for idelta = 1:length(values_delta)
        B_l = subs(B_l_fun, var.delta, values_delta(idelta));
        F_t(i,idelta) = subs(F_t_fun, var.delta, values_delta(idelta));

        F_t_cosine(i,idelta) = MV0(MV == var.vertical_load) * MV0(MV == var.phi_x) * cos(values_delta(idelta)); % Calculate the lateral force on the castor wheel.

        M_D(i,idelta) = norm(double(subs(M_D_fun, var.delta, values_delta(idelta)))); % Calculate the magnitude of the moment on the swivel bearing, expressed in triad F (Fork fixed).

        delta_list(idelta) = atan2(B_l(2),B_l(1)); 
    end

    b = plot(values_delta, F_t_cosine(i,:), '--', 'Color',[1;1;1]*0.8, 'DisplayName',"Cosine approximation");
    a = plot(delta_list, F_t(i,:), '-', 'Color',col(2*i-1,:), 'DisplayName',"Simulated");
    
    if i == 4
        a_legend = a;
        b_legend = b;
    end

    text(delta_list(end), F_t(end)', "\hspace{2mm}" + name_iterated_variable + "=" + values_phi_x(i), 'Interpreter','latex',FontSize=8)
end

% Add experimental data
T_data_1 = readtable("../Data/Data_matrices.xlsx", "Range","C5:E65", "ReadVariableNames",true);
T_data_2 = readtable("../Data/Data_matrices.xlsx", "Range","C70:E85", "ReadVariableNames",true);

T_data_1.F_t_norm = T_data_1.F_t./T_data_1.F_n;

values_delta_experiment = unique(T_data_2.delta);
c = plot(deg2rad(T_data_2.delta), T_data_2.F_t_norm, 'x', 'DisplayName', "Experimental at $\varphi_x = 0.1$ rad", Color=col(7,:));
l = plot(deg2rad(values_delta_experiment), (T_data_2.F_t_norm' * (values_delta_experiment' == T_data_2.delta) /3)', ':', 'Color', col(7,:), 'DisplayName',"Mean of experimental results");

indices_delta_experiment = [];
RMSE_experiment_vs_simulation = [];

% Match experimental data to the closest simulation solution
for i = 1:length(unique(T_data_2.delta))
    trials_selected = T_data_2(T_data_2.delta == values_delta_experiment(i), :);

    % Find the simulatoin indices that most closely match the experimental tests
    [~,indices_delta_experiment(i,1)] = min(abs(rad2deg(values_delta) - values_delta_experiment(i)));
    [~,index_cant01] = min(abs(values_phi_x - 0.1));

    % Calculate RMSE between the three experimental samples and the simulation result
    RMSE_experiment_vs_simulation(i,1) = sqrt( (1/size(trials_selected,2)) * sum( ( trials_selected.F_t_norm - F_t(index_cant01,indices_delta_experiment(i)) ).^2 ) )

    % Print RMSE result
    text(deg2rad(values_delta_experiment(i)), F_t(index_cant01,indices_delta_experiment(i)) - 0.05, num2str(RMSE_experiment_vs_simulation(i,1),'%.3f'), 'Interpreter','latex', 'HorizontalAlignment', 'center', "FontSize",8)
end

% Function to calculate RMSE
rmse = @(a,b) sqrt(1/length(a) * sum((a - b).^2))

% Compare cosine and full simulation
RMSE_cosine_vs_simulation = rmse(F_t', F_t_cosine')
max_cosine_vs_simulation = max(abs(F_t - F_t_cosine),[],2)

% Add titles and labels
% title("Lateral castor force $F_t$ for different cant angles $\varphi_x$", 'Interpreter','latex')
xlabel(name, 'Interpreter','latex')
ylabel('Normalized lateral force $F_t / F_n$', 'Interpreter','latex')
xlim([-pi,pi])
xticks([-pi, -0.5*pi, 0, 0.5*pi, pi])
xticklabels({'-$\pi$', '-$\pi$/2', '0', '$\pi$/2', '$\pi$'})
ylim([-0.3,0.3])
legend([a_legend,b_legend,l,c], 'Interpreter','latex',Box='off',Location='southoutside')

% Figure of cant angle vs normal force
switch figureLayoutMode
    case "tiled"
        ax2 = nexttile; hold on
        linkaxes([ax1, ax2], 'y')
    otherwise
        figure; hold on
end


%         F_t |
%         --- |
%         F_n |
%             |____________
%                  F_n

T_data_1.F_t_cosine = 1 * T_data_1.phi_x * cos(0);

phi_x_unique = unique(T_data_1.phi_x);
for i = 1:length(phi_x_unique)
    trials_selected = T_data_1(T_data_1.phi_x == phi_x_unique(i), :);
    target = trials_selected.F_t_cosine(1);
    s = scatter(trials_selected.F_n, trials_selected.F_t_norm, 'x', 'MarkerEdgeColor', col(4+i,:), "DisplayName","Experimental results for each $\varphi_x$");

    MV0 = sym(var.MV0);
    MV0(var.MV == var.phi_x) = phi_x_unique(i);

    q0 = sym(zeros(size(var.q_sym)));
    q0(var.q_sym == var.delta) = 0;

    F_t_sim = double(methods_1_fun.C_F_C_y(q0, MV0));

    b = plot([0;80],[target;target], '--', 'Color', [1;1;1]*0.8, "DisplayName","Cosine approximation");
    a = plot([0;80],[F_t_sim;F_t_sim], 'Color', col(4+i,:), "DisplayName","Simulated");

    F_n_list = sort(unique(trials_selected.F_n));
    l = plot(F_n_list, (trials_selected.F_t_norm' * (F_n_list' == trials_selected.F_n) /3)', ':', 'Color', col(4+i,:), "DisplayName","Mean of experimental results");

    text(80, target, "\hspace{2mm}" + name_iterated_variable + "=" + target, 'Interpreter','latex',FontSize=8)

    if i == 3
        a_legend = a;
        b_legend = b;
        s_legend = s;
        l_legend = l;
    end
end

set(gca,'TickLabelInterpreter','latex')
% posfig=get(gca,'position');  % retrieve the current values
% posfig(3)=0.95*posfig(3);    % reduce width
% set(gca,'position',posfig);  % write the new values
% title("Normal force $F_n$ and cant angle $\varphi_x$", 'Interpreter','latex')
xlabel("Normal force $F_n$ (N)", 'Interpreter','latex')

ax2.YAxis.Visible = 'off'; % remove x-axis
ylabel('Normalized lateral force $F_t / F_n$', 'Interpreter','latex')

xlim([0, max(T_data_1.F_n)])
ylim([-0.3,0.3])
xlim([0,80])
legend([a_legend,b_legend,l_legend,s_legend], 'Interpreter','latex',Box='off',Location='southoutside')
xticks([0, 19.8, 39.3, 57.5, 75.5])


CastorFigure_save_figureXYZiso([], path + "/F_t", 2)


