function CastorFigure_printcastor(var,kin_fun,q, MVi)
% printcastor prints a castor wheel including the wheel circle for the state q
addpath("..\functions\")

kin = Structconv_fun2num(kin_fun, q, MVi);
color = TUD_color_generate();

% Frame lines
LinePlot3([kin.C,kin.A]','--r')
LinePlot3([kin.A,kin.A2]','k')
LinePlot3([kin.A2,kin.F]','k')
LinePlot3([kin.F,kin.D]','k')

% print wheel
theta_plot = linspace(0,2*pi);

for i = 1:length(theta_plot)
    N_tire_arc(:,i) = kin.A + kin.NrotF * roty(theta_plot(i))*kin.NrotF'*kin.r;
end

plot3(N_tire_arc(1,:)',N_tire_arc(2,:)',N_tire_arc(3,:)','b')
TriadPlot3(kin.B,kin.NrotB,var.Ltriad,'Color',color.B)
TriadPlot3(kin.D,kin.NrotD,var.Ltriad,'Color',color.D)
TriadPlot3(kin.C,kin.NrotC,var.Ltriad,'Color',color.C)
TriadPlot3(kin.CoMF,kin.NrotF,var.Ltriad,'Color',color.F)
% TriadPlot3(kin.A(q),kin.NrotA,var.Ltriad,'Color',color.A)

% LinePlot3(kin.R','go')
LinePlot3([kin.T, kin.A]','m')
end

