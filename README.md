# Supplementary information for publication "A slanted castor wheel enables pushing manual wheelchairs from the side to improve social interaction". 

**DOI database**: doi.org/10.4121/82afa4e1-bdf1-4bf4-8682-32e62f6c3d68 

**DOI publication**: doi.org/10.1371/journal.pone.0307759

**License**: Available under CC BY 4.0 license.

**Supplementary information**: This repository contains the supplementary S2 files, and supplementary videos S3-5.

## S2 File. Experimental data and the MATLAB code for calculations of castor wheel.
### CastorModel
Contains Matlab code for the simulation of the castor wheel and the generation of figures comparing simulations to experimental results. This code relies on the `functions` folder. To run the code and generate the results figure presented in the paper, run the `main.m` file.

### Data
Contains experimental data of the castor wheel on the treadmill, organized by collection date.

### functions
Contains functions used by `CastorModel` code.

### Rolling resistance
Contains Matlab code to compute the rolling resistance of the castor wheel on the treadmill. The value of the computed rolling resistance is used in the `CastorModel`. 

## S3 Video. Video of the control wheelchair on the treadmill.
The video contains the control conditions, where the wheelchair was pulled using a cable while rolling on a treadmill.

![The wheelchair on a treadmill in the control condition without cant angle](S3 Video - Video of the control wheelchair on the treadmill.mp4)

## S4 Video. Video of the test wheelchair on the treadmill.
The video contains the test condition, where the wheelchair with a slanted castor wheel was pulled using a cable attached to the push bar while rolling on a treadmill.

![The wheelchair on a treadmill in the test condition with cant angle](S4 Video - Video of the test wheelchair on the treadmill.mp4)

## S5 Video. Video of the wheelchair during outdoor use with an occupant.
The video demonstrates how a modified wheelchair could be used outdoors on an uneven path, allowing the occupant and caregiver to walk side by side.

![Wheelchair with occupant during outdoor use](S5 Video - Video of the wheelchair during outdoor use with an occupant.mp4)

## Snapshots and related figures.
Here static snapshots of the supplementary videos are provided. These snapshots can be found and downloaded from the `Supplementary video figures` folder.

### S3 snapshot
![S3: The wheelchair on a treadmill in the control condition without cant angle](Supplementary\ video\ figures/S3_scene1.png)

### S4 snapshot
![S4: The wheelchair on a treadmill in the test condition with cant angle](Supplementary\ video\ figures/S4_scene1.png)

### S5 snapshots
![S5: Wheelchair with occupant during outdoor use, scene 1](Supplementary\ video\ figures/S5_scene1.png)
![S5: Wheelchair with occupant during outdoor use, scene 2](Supplementary\ video\ figures/S5_scene2.png)

### Additional figure of the wheelchair from the front
![Wheelchair with occupant during outdoor use, additional figure](Supplementary\ video\ figures/Wheelchair_outdoor_front.JPG)



